FROM rust:1.78.0-buster AS base
WORKDIR /app

FROM base AS tooling
RUN cargo install cargo-chef && \
    cargo install sccache && \
    cargo install just && \
    cargo install cross --git https://github.com/cross-rs/cross

FROM tooling AS planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

FROM tooling AS build
ENV SCCACHE_CACHE_SIZE="5G"
ENV SCCACHE_DIR=/cache/sccache
ENV RUSTC_WRAPPER=/usr/local/cargo/bin/sccache
COPY --from=planner /app/recipe.json recipe.json
RUN --mount=type=cache,target=/cache/sccache cargo chef cook --recipe-path recipe.json
RUN --mount=type=cache,target=/cache/sccache cargo chef cook --recipe-path recipe.json --release
COPY . .
RUN --mount=type=cache,target=/cache/sccache cargo build && sccache --show-stats

WORKDIR /app
RUN ls -alh
