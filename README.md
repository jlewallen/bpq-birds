# What is this?

Well, the short version is that this is a linBPQ application for allowing users
to see which birds have been detected by my local
[BirdNET-PI](https://github.com/mcguirepr89/BirdNET-Pi) installation.

I am now realizing that's sort of an acronym soup, so the longer version.

The most important part is BirdNET-PI, which is sofwtare that's installed on a
local Raspberry PI with a USB microphone and listens for bird calls. It runs a
web server one can visit to see which birds have been detected.

The next major piece is [linBPQ](https://github.com/g8bpq/linbpq), which is a
packet BBS for amateur radio nerds, like myself. I run one from my home in
Southern California on 2 meters.

# Instructions

**You will need working LinBPQ and BirdNET-PI installations.**

The integration is fairly simple. The `bpq-birds` server listens for incoming TCP connections on port 4565. When a user enters ths "Application Command" on your BPQ node, it will connect to this port and immediately send the callsign of the user, and then continue to forward anything received over that client connection. Anything sent by the `bpq-birds` server to the client gets forwarded to the user.

The `bpq-birds` server needs to be installed and running on the same machine as `LinBPQ`. I do this by running the server under systemd. Configuration wise, the environment variable `BIRD_API` should be the root URL of the BirdNet-API server.

There are two parts to the BPQ configuration. The first is the `CMDPORT` directive. This is a space separated list of ports. I don't use any others on my installation and so mine looks like this:

`CMDPORT=4565`

Next is the `APPLICATION` line and will depend on your installation. I only run a BBS, which occupies `APPLICATION 1` and so I use `APPLICATION 2` for this:

`APPLICATION 2,BIRDS,C 5 HOST 0 S`

Breaking this down:

`BIRDS` is the command visitors will enter.
`C 5 HOST 0 S` is a "CONNECT" command, where `5` is the port of your telnet port, `HOST` tells BPQ to connect to the host machine, and `0` is the port number from the `CMDPORT` directive, which in my case is 0 since it's the only one. `S` is optional, and indicates the user should be returned to the noded when they quit the application.

You can find some more information here: [LinBPQ Applications Interface](https://www.cantab.net/users/john.wiseman/Documents/LinBPQ%20Applications%20Interface.html) **Note:** `bpq-birds` does not use `inetd` to start, so that portion is not relevant.
