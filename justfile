artifacts:
	echo rustup target add aarch64-unknown-linux-gnu
	mkdir -p artifacts
	cargo build
	cross build --target aarch64-unknown-linux-gnu
	cp target/debug/bpq-birds artifacts/bpq-birds-x86-64
	cp target/aarch64-unknown-linux-gnu/debug/bpq-birds artifacts/bpq-birds-aarch64
