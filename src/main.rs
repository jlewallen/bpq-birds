use anyhow::Result;
use chrono::{DateTime, NaiveTime, Utc};
use clap::{command, Parser, Subcommand};
use futures::{sink::SinkExt, StreamExt};
use scraper::{ElementRef, Html, Selector};
use serde::Deserialize;
use std::collections::HashMap;
use thiserror::Error;
use tokio::net::{TcpListener, TcpStream};
use tokio_util::codec::{Framed, LinesCodec};
use tracing::*;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

#[derive(Deserialize, Debug)]
struct RecentlyResponse {
    detections: Vec<Recently>,
}

#[derive(Deserialize, Debug, Clone)]
struct Recently {
    when: DateTime<Utc>,
    // file_name: String,
    common_name: String,
    confidence: f32,
    // spectrogram_url: String,
    // audio_url: String,
    // available: Option<bool>,
}

struct Statistics {
    name: String,
    last: NaiveTime,
    detections: usize,
    total_confidence: f32,
}

struct BirdService {
    url: String,
}

struct Reply(String);

impl Into<Reply> for RecentlyResponse {
    fn into(self) -> Reply {
        let mut summaries: HashMap<String, Statistics> = HashMap::new();

        for d in self.detections.into_iter() {
            let when = d.when.time();
            let s = summaries
                .entry(d.common_name.clone())
                .or_insert(Statistics {
                    name: d.common_name,
                    last: when,
                    detections: 0,
                    total_confidence: 0.0,
                });
            s.detections += 1;
            s.total_confidence += d.confidence;
            if s.last < when {
                s.last = when;
            }
        }

        let mut summaries: Vec<Statistics> = summaries.into_values().collect();

        summaries.sort_by_key(|s| s.last);

        summaries.reverse();

        let mut reply = String::new();

        reply.push_str("Todays birds detected via my local BirdNET-PI:\n");
        reply.push_str("https://github.com/mcguirepr89/BirdNET-Pi\n\n");

        for summary in summaries.into_iter() {
            reply.push_str(&format!(
                "{:32} {:4} times last @ {:?} ({:.2}% avg confidence)\n",
                summary.name,
                summary.detections,
                summary.last,
                (summary.total_confidence / (summary.detections as f32) * 100.0)
            ));
        }

        Reply(reply)
    }
}

impl BirdService {
    pub fn new() -> Result<Self> {
        Ok(Self {
            url: std::env::var("BIRD_API")?,
        })
    }

    #[allow(dead_code)]
    /// This uses my custom API and requires an additional service with access to the database.
    pub async fn todays(&self) -> Result<Reply> {
        let recently = self.url.clone() + "/recently.json";
        info!("querying {:?}", &recently);

        let body: RecentlyResponse = reqwest::get(recently).await?.json().await?;

        Ok(body.into())
    }

    pub async fn last_n(&self, n: usize) -> Result<Reply> {
        let path = format!(
            "/todays_detections.php?ajax_detections=true&hard_limit={}",
            n
        );
        let recently = self.url.clone() + path.as_str();
        info!("querying {:?}", &recently);

        let body: String = reqwest::get(recently).await?.text().await?;
        let html = Html::parse_document(&body);
        let select_tr = Selector::parse("tr").unwrap();
        let detections: Vec<DetectionRow> = html
            .select(&select_tr)
            .flat_map(|tr| tr.try_into().ok())
            .collect();

        Ok(detections.into())
    }
}

#[derive(Debug)]
#[allow(dead_code)]
struct DetectionRow {
    common_name: String,
    scientific_name: String,
    confidence: usize,
    time: NaiveTime,
}

impl<'a> TryFrom<ElementRef<'a>> for DetectionRow {
    type Error = DetectionParseError;

    fn try_from(value: ElementRef) -> Result<Self, Self::Error> {
        let select_names = Selector::parse("a.a2").unwrap();
        let confidence_pattern = regex::Regex::new(r"(\d+)%").unwrap();
        let time_pattern = regex::Regex::new(r"(\d+:\d+:\d+)").unwrap();

        let names = value
            .select(&select_names)
            .flat_map(|e| e.text().collect::<Vec<_>>())
            .collect::<Vec<_>>();

        let maybe_time = value
            .text()
            .flat_map(|text| time_pattern.captures(&text))
            .next()
            .map(|c| NaiveTime::parse_from_str(&c[1], "%H:%M:%S").unwrap());
        let maybe_confidence: Option<usize> = value
            .text()
            .flat_map(|text| confidence_pattern.captures(&text))
            .next()
            .map(|c| c[1].parse().unwrap());

        if names.len() != 2 {
            Err(DetectionParseError::NotEnoughNames)
        } else {
            Ok(Self {
                common_name: names[0].to_owned(),
                scientific_name: names[1].to_owned(),
                confidence: maybe_confidence.ok_or(DetectionParseError::NoConfidence)?,
                time: maybe_time.ok_or(DetectionParseError::NoTime)?,
            })
        }
    }
}

#[derive(Debug, Error)]
enum DetectionParseError {
    #[error("not enough names")]
    NotEnoughNames,
    #[error("no confidence")]
    NoConfidence,
    #[error("no time")]
    NoTime,
}

impl Into<Reply> for Vec<DetectionRow> {
    fn into(self) -> Reply {
        let mut summaries: HashMap<String, Statistics> = HashMap::new();

        for d in self.into_iter() {
            let s = summaries
                .entry(d.common_name.clone())
                .or_insert(Statistics {
                    name: d.common_name,
                    last: d.time,
                    detections: 0,
                    total_confidence: 0.0,
                });
            s.detections += 1;
            s.total_confidence += d.confidence as f32;
            if s.last < d.time {
                s.last = d.time;
            }
        }

        let mut summaries: Vec<Statistics> = summaries.into_values().collect();

        summaries.sort_by_key(|s| s.last);

        summaries.reverse();

        let mut reply = String::new();

        reply.push_str("Last 100 bird detections from my BirdNET-PI:\n");
        reply.push_str("https://github.com/mcguirepr89/BirdNET-Pi\n\n");

        for summary in summaries.into_iter() {
            reply.push_str(&format!(
                "{:32} {:4} times last @ {:?} ({:.2}% avg confidence)\n",
                summary.name,
                summary.detections,
                summary.last,
                (summary.total_confidence / summary.detections as f32)
            ));
        }

        Reply(reply)
    }
}

async fn process(stream: TcpStream) -> Result<()> {
    let mut stream = Framed::new(stream, LinesCodec::new_with_max_length(256));

    let Ok(service) = BirdService::new() else {
        stream.send("service unavailable, sorry :(").await?;
        return Ok(());
    };

    while let Some(Ok(line)) = stream.next().await {
        info!("received {:?}", &line);

        match line.trim().to_lowercase().as_str() {
            "" => {}
            "help" => {
                stream
                    .send("Everything except for BYE will show the birds table.")
                    .await?;
            }
            "bye" => {
                stream.send("73").await?;
                break;
            }
            _ => {
                stream.send(service.last_n(100).await?.0).await?;
            }
        }
    }

    Ok(())
}

#[derive(Subcommand)]
enum Commands {
    Query,
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Options {
    #[arg(short, long, action = clap::ArgAction::Count)]
    verbose: u8,
    #[command(subcommand)]
    command: Option<Commands>,
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(get_rust_log()))
        .with(tracing_subscriber::fmt::layer())
        .init();

    let options = Options::parse();

    match options.command {
        Some(_) => query().await,
        None => listen().await,
    }
}

async fn query() -> Result<()> {
    let service = BirdService::new()?;

    let todays = service.last_n(100).await?;

    println!("{}", todays.0);

    Ok(())
}

async fn listen() -> Result<()> {
    info!("listening on 0.0.0.0:4565");

    let listener = TcpListener::bind("0.0.0.0:4565").await?;

    loop {
        let (socket, from) = listener.accept().await?;

        info!("{:?} connected", from);

        tokio::spawn(async move {
            match process(socket).await {
                Err(e) => warn!("{:?}", e),
                Ok(_) => info!("{:?} gone", from),
            }
        });
    }
}

fn get_rust_log() -> String {
    std::env::var("RUST_LOG").unwrap_or_else(|_| "debug,html5ever=info,selectors=info".into())
}
